package gesture;

import geometry.Point2D;

import java.util.ArrayList;

public abstract class GestureRecognizer {

	private GestureRecognizerListener listener;
	
	protected ArrayList<Point2D> points = new ArrayList<Point2D>();
	
	/**
	 * @param id - the cursor's id
	 * @param x
	 * @param y
	 * @param type - the event type as defined in MultitouchFramework.TOUCH_EVENT_***
	 */
	public abstract void onTouchEvent(int id, float x, float y, int type );

	public abstract String gestureName();
	
	public void detectGesture(GestureData data){
		if(listener != null){
			listener.didRecognizeGesture(gestureName(), data);
		}
	}
	
	public void setListener(GestureRecognizerListener listener){
		this.listener = listener;
	}
	
	public interface GestureRecognizerListener{
		void didRecognizeGesture(String gesture, GestureData data);
	}
}