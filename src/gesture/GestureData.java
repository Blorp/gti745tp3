package gesture;

public class GestureData {
	public float startx;
	public float starty;
	public float endx;
	public float endy;
	public float width;
	public float height;
	public float angle;
	//other data if necessary

}
