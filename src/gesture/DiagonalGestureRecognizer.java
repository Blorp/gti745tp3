package gesture;

import main.MultitouchFramework;
import geometry.Point2D;
import geometry.Vector2D;

public class DiagonalGestureRecognizer extends GestureRecognizer {
	
	private final float LINE_DETECTION_MIN_LENGTH = 10f;
	private final float LINE_DETECTION_TOLERANCE= 0.1f;
	private final float ANGLE_TOLERANCE = 0.25f;
	private Point2D pstart = null;

	private Vector2D line = null;
	private int cursorId = -1;

	@Override
	public void onTouchEvent(int id, float x, float y, int type) {
		switch(type){
		case MultitouchFramework.TOUCH_EVENT_DOWN:
			if(cursorId == -1){
				cursorId = id;
			}
			break;
		case MultitouchFramework.TOUCH_EVENT_MOVE:
			if(cursorId == id){
				Point2D p = new Point2D(x,y);
				points.add(p);
			}
			break;
		case MultitouchFramework.TOUCH_EVENT_UP:
			if(cursorId == id){
				
				if(id != cursorId){
					return;
				}

				if(points.isEmpty()) return;

				if(pstart == null && line == null){
					pstart = points.get(0);
				}
				
				Point2D firstPoint = points.get(0);
				Point2D lastPoint = points.get(points.size()-1);

				float straightLineDistance = firstPoint.distance(lastPoint);
				if(straightLineDistance < LINE_DETECTION_MIN_LENGTH){
					return;
				}

				float pointByPointDistance = 0;
				for(int i=1; i<points.size(); i++){
					pointByPointDistance += points.get(i-1).distance(points.get(i));
				}

				
				if(pointByPointDistance < straightLineDistance*(1.0f+LINE_DETECTION_TOLERANCE)){
					line = Point2D.diff(points.get(points.size()-2), firstPoint);
					Point2D pend = points.get(points.size()-1);
					points.clear();
					//System.out.println("angle: " + line.angle()  + " tolerance min " + ((Math.PI/4)-ANGLE_TOLERANCE) + "tolerance max" + ((Math.PI/4)+ANGLE_TOLERANCE));
					if(line.angle() > ((Math.PI/4)-ANGLE_TOLERANCE) && line.angle() < ((Math.PI/4)+ANGLE_TOLERANCE)){
						System.out.println("Detect diagonal");
						GestureData data = new GestureData();
						
						if(firstPoint.x() < lastPoint.x()){
							data.startx = firstPoint.x();
						}else{
							data.startx = lastPoint.x();
						}
						
						if(firstPoint.y() < lastPoint.y()){
							data.starty = firstPoint.y();
						}else{
							data.starty = lastPoint.y();
						}
						
						data.width = Math.abs(firstPoint.x() - lastPoint.x());
						data.height = Math.abs(firstPoint.y() - lastPoint.y());
						
						this.detectGesture(data);
					}
				}
				reset();
			}
			break;
		}

	}
	
	private void reset(){
		line = null;
		pstart = null;
		points.clear();
		cursorId = -1;
	}
	

	@Override
	public String gestureName() {
		return "DiagonalGesture";
	}

}
