package gesture;

import geometry.Point2D;
import geometry.Vector2D;
import main.MultitouchFramework;

public class LGestureRecognizer extends GestureRecognizer {

	private final float LINE_DETECTION_MIN_LENGTH = 100f;
	private final float LINE_DETECTION_TOLERANCE= 0.1f;

	private Vector2D firstLine = null;
	private Vector2D secondLine = null;
	private int cursorId = -1;
	private Point2D pstart = null;

	@Override
	public void onTouchEvent(int id, float x, float y, int type) {
		switch(type){
		case MultitouchFramework.TOUCH_EVENT_DOWN:
			if(cursorId == -1){
				cursorId = id;
			}
			break;
		case MultitouchFramework.TOUCH_EVENT_MOVE:
			if(cursorId == id){
				Point2D p = new Point2D(x,y);
				points.add(p);
			}
			break;
		case MultitouchFramework.TOUCH_EVENT_UP:
			if(cursorId == id){
				if(firstLine != null && points.size() > 0){
					Point2D pend = points.get(points.size()-1);
					secondLine = Point2D.diff(pend, points.get(0));
					if(secondLine.length() > LINE_DETECTION_MIN_LENGTH){
						boolean angleBetweenFirstAndSecondNear90 = false;
						float firstAngle = firstLine.angle();
						float secondAngle = secondLine.angle();
//						System.out.println("FIRST ANGLE - " + firstAngle * 180.0f/(Math.PI));
//						System.out.println("SECOND ANGLE - " + secondAngle * 180.0f/(Math.PI));
						float angleDiff = Math.abs(secondAngle - firstAngle);
						angleDiff = (float) Math.min(angleDiff, 2*Math.PI - angleDiff);
						float PI_2 = (float) (Math.PI / 2.0f);
						angleBetweenFirstAndSecondNear90 = angleDiff > PI_2*(0.9f) && angleDiff < PI_2*(1.1f);
						if(angleBetweenFirstAndSecondNear90){
							GestureData data = new GestureData();
							data.startx = pstart.x();
							data.starty = pstart.y();
							data.width = secondLine.length();//Math.abs(pend.x() - pstart.x()); 
							data.height = firstLine.length();//Math.abs(pend.y() - pstart.y());
							data.angle = secondAngle;
							this.detectGesture(data);
						}
					}
					reset();
				}else{
					reset();
				}
			}
			break;
		}

		if(id != cursorId){
			return;
		}

		if(points.isEmpty()) return;

		if(pstart == null && firstLine == null){
			pstart = points.get(0);
		}

		Point2D firstPoint = points.get(0);
		Point2D lastPoint = points.get(points.size()-1);

		float straightLineDistance = firstPoint.distance(lastPoint);
		if(straightLineDistance < LINE_DETECTION_MIN_LENGTH){
			return;
		}

		float pointByPointDistance = 0;
		for(int i=1; i<points.size(); i++){
			pointByPointDistance += points.get(i-1).distance(points.get(i));
		}

		if(pointByPointDistance > straightLineDistance*(1.0f+LINE_DETECTION_TOLERANCE)){ // line broke
			if(firstLine == null){
				firstLine = Point2D.diff(points.get(points.size()-2), firstPoint);
				points.clear();
				System.out.println("DID DETECT FIRST LINE");
			}else{
				reset();
			}
		}

		//		float hypoSquared = firstLine.lengthSquared() + secondLine.lengthSquared();
		//		float straightHypoSquared = Point2D.diff(pend, pstart).lengthSquared();
		//		float min = straightHypoSquared*(1.0f-LINE_DETECTION_TOLERANCE);
		//		float max = straightHypoSquared*(1.0f+LINE_DETECTION_TOLERANCE);
		//		angleBetweenFirstAndSecondNear90 = hypoSquared > min && hypoSquared < max;

		//		Vector2D newLine = Point2D.diff(lastPoint, firstPoint);
		//		
		//		if(lastComputedLine == null || lastPoints.size() < 10){
		//			lastComputedLine = newLine;
		//			return;
		//		}
		//		
		//		float lastAngle = lastComputedLine.angle();
		//		float newAngle = newLine.angle();
		//		float angleDiff = Math.abs(newAngle - lastAngle);
		//		System.out.println("LAST ANGLE - " + lastAngle * ((2*Math.PI)/360));
		//		System.out.println("NEW ANGLE - " + newAngle * ((2*Math.PI)/360));
		//		angleDiff = (float) Math.min(angleDiff, 2*Math.PI - angleDiff);
		//		
		//		System.out.println("ANGLE DIFF - " + angleDiff * ((2*Math.PI)/360));
		//		
		//		float THRESHOLD = (float) (2*Math.PI * 0.1f);
		//		if(angleDiff > THRESHOLD){ // line broke
		//			System.out.println("ANGLE DIFF TOO HIGH : ANGLE - " + angleDiff * ((2*Math.PI)/360));
		//			if(lastComputedLine.lengthSquared() < LINE_DETECTION_MIN_LENGTH_SQUARED){ // not long enough
		//				System.out.println("LINE NOT LONG ENOUGH - LENGTH = " + lastComputedLine.lengthSquared());
		//				reset();
		//			}else{ // line long enough, create new line
		//				if(firstLine == null){
		//					firstLine = lastComputedLine;
		//					lastPoints.clear();
		//					System.out.println("DID DETECT FIRST LINE - Angle Diff : " + angleDiff);
		//				}else{
		//					secondLine = lastComputedLine;
		//					System.out.println("DID DETECT SECOND LINE - Angle Diff : " + angleDiff);
		//				}
		//			}
		//			lastComputedLine = null;
		//		}else{
		//			lastComputedLine = newLine;
		//		}
	}

	private void reset(){
		firstLine = null;
		secondLine = null;
		pstart = null;
		points.clear();
		cursorId = -1;
		System.out.println("DID CANCEL L");
	}

	@Override
	public String gestureName() {
		return "LGesture";
	}
}
