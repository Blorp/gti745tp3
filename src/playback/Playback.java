
package playback;

import java.util.ArrayList;

import javax.sound.midi.Instrument;
import javax.sound.midi.MidiChannel;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Synthesizer;

import tracking.NoteCue;
import tracking.Track;

public class Playback {
	
	public int volume = 80;
	private Synthesizer synth;
	private MidiChannel[] channels;
	private Instrument[] instruments;

	private static Playback instance;
	private Playback(){};
	public static Playback getInstance(){
		if(instance == null){
			instance = new Playback();
			instance.init();
		}
		return instance;
	}
	
	public void init(){
		try {
			synth = MidiSystem.getSynthesizer();
			synth.open();
			channels = synth.getChannels();
			instruments = synth.getAvailableInstruments();
		} catch (MidiUnavailableException e) {
			e.printStackTrace();
		}
	}
	
	public void finish(){
		synth.close();
	}
	
	public int getPianoChannel(){
		return 0;
	}
	
	public void setInstrument(int instrument, int channel) {
		//synth.loadInstrument(instruments[instrument]);
		channels[channel].programChange(instruments[instrument].getPatch().getProgram());
	}

	public void play(final Note note, final long duration){
		
		if(isRecording){
			NoteCue cue = new NoteCue(note, duration, System.currentTimeMillis() - this.startTime);
			this.cues.add(cue);
		}
		Runnable r = new Runnable(){
			@Override
			public void run() {
				channels[note.channel].noteOn(note.number, volume);
				try {
					synchronized(this){
						this.wait(duration);
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				} finally {
					channels[note.channel].noteOff(note.number,volume);
				}
			}
		};
		new Thread(r).start();
	}
	
	public void play(final Note note){
		if(isRecording){
			NoteCue cue = new NoteCue(note, 0, System.currentTimeMillis() - this.startTime);
			this.cues.add(cue);
		}
		channels[note.channel].noteOn(note.number, volume);
	}
	
	public void stop(final Note note){
		if(isRecording){
			this.setEndTimeForLastNoteFromArray(note, System.currentTimeMillis());
		}
		channels[note.channel].noteOff(note.number,volume);
		
	}
	
	
	
	//RECORDING
	public boolean isRecording = false;
	public Track track;
	long startTime;
	ArrayList<NoteCue> cues;
	
	
	public void startRecording(){
		System.out.println("Starting Recording");
		if(!isRecording){
			this.isRecording = true;
			this.cues = new ArrayList<NoteCue>();
			this.startTime = System.currentTimeMillis();
		}
		
	}
	
	public Track stopRecording(){
		System.out.println("Stopping Recording");
		if(isRecording){
			this.isRecording = false;
			this.track = new Track(System.currentTimeMillis() - this.startTime);
			for(int i=0; i<this.cues.size();i++){
				this.track.add(this.cues.get(i));
			}
			return this.track;
		}
		return null;
	}
	
	public void setEndTimeForLastNoteFromArray(Note note, long endTime){
		
		for(int i = this.cues.size()-1; i>=0; i--){
			NoteCue nc = this.cues.get(i);
			if(nc.note.equals(note)){
				nc.setDurationForLastCueOfNote(endTime-this.startTime);
				break;
			}
		}
	}
}
