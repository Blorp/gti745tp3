package playback;

public class Note {

	public int number;
	public int channel;

	public static int LOWEST_C = 24;
	public static String[] NAMES = {"C","","D","","E","F","","G","","A","","B"};
	private final static String[] FULL_NAMES = {"C","C#","D","D#","E","F","F#","G","G#","A","A#","B"};

	public boolean isWhite(){
		int scaledDown = number % 12;
		if(scaledDown == 5) return true;
		if(scaledDown == 10) return false;
		return (scaledDown < 5 && scaledDown % 2 ==0) || (scaledDown > 5 && scaledDown % 2 != 0);
	}

	@Override
	public String toString() {
		int mod = number % 12;
		if(mod < 12){
			return FULL_NAMES[mod];
		}else{
			return "";
		}
	}
	
	public Note(){}
	
	public Note(int number, int channel){
		this.number = number;
		this.channel = channel;
	}

	public static Note get(String format){
		return Note.get(format, 0);
	}

	public static Note get(String format, int channel){
		if(format == null || format.length() < 2 || format.length() > 3){
			return null;
		}

		format = format.toUpperCase();

		String name = format.substring(0, 1);

		int nameIndex = -1;
		for(int i = 0; i < NAMES.length; i ++){
			if(NAMES[i].equals(name)){
				nameIndex = i;
				break;
			}
		}

		if(nameIndex == -1){
			return null;
		}

		int scale = Integer.parseInt(format.substring(1,2));
		int n = LOWEST_C + 12*scale + nameIndex;

		Note note = new Note();
		note.number = n;
		note.channel = channel;

		if(format.endsWith("#")){
			note.number++;
		}else if(format.endsWith("B")){
			note.number--;
		}

		return note;
	}
	
	
	public static Note get(int number, int channel){

		Note note = new Note();
		note.number = number;
		note.channel = channel;

		return note;
	}
}
