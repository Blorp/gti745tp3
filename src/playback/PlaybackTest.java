package playback;

import tracking.Track;

public class PlaybackTest {

	public static void main(String[] args) throws InterruptedException {
		int i = 0;
		//while(true){
			i++;
			//THE LAST METROID IS IN CAPTIVITY
			if(i%4==0){
				Playback.getInstance().play(Note.get("F6#", 15), 2000);
			}
			Playback.getInstance().startRecording();
			//Playback.getInstance().setInstrument(127, 15); // Set gun shot instrument (#128) to channel 16
			Playback.getInstance().play(Note.get("C3#", 15), 250);	Thread.sleep(150);
			Playback.getInstance().play(Note.get("C3#", 15), 250);	Thread.sleep(150);

			Playback.getInstance().play(Note.get("D3#", 15), 250);	Thread.sleep(150);
			Playback.getInstance().play(Note.get("D3#", 15), 150);	Thread.sleep(150);

			Playback.getInstance().play(Note.get("F3#", 15), 250);	Thread.sleep(150);
			Playback.getInstance().play(Note.get("F3#", 15), 150);	Thread.sleep(150);

			Playback.getInstance().play(Note.get("F3", 15), 250);	Thread.sleep(150);
			Playback.getInstance().play(Note.get("F3", 15), 150);	Thread.sleep(150);
			
			//Playback.getInstance().setInstrument(123, 5); // Set bird instrument (#124) to channel 6
			Playback.getInstance().play(Note.get("G3#", 5), 250);	Thread.sleep(150);
			Playback.getInstance().play(Note.get("G3#", 5), 150);	Thread.sleep(150);

			Playback.getInstance().play(Note.get("F3#", 5), 250);	Thread.sleep(150);
			Playback.getInstance().play(Note.get("F3#", 5), 150);	Thread.sleep(150);

			Playback.getInstance().play(Note.get("B3", 5), 250);	Thread.sleep(150);
			Playback.getInstance().play(Note.get("B3", 5), 150);	Thread.sleep(150);

			Playback.getInstance().play(Note.get("A3#", 5), 250);	Thread.sleep(150);
			Playback.getInstance().play(Note.get("A3#", 5), 150);	Thread.sleep(150);
			//THE GALAXY IS AT PEACE
			if(i%4==0){
				Playback.getInstance().play(Note.get("F6", 15), 2000);
			}
			Track t = Playback.getInstance().stopRecording();
			t.play();
		//}
	}
}
