package main;

import geometry.AlignedRectangle2D;
import geometry.Point2D;
import geometry.Point2DUtil;
import gesture.DiagonalGestureRecognizer;
import gesture.GestureData;
import gesture.GestureRecognizer;
import gesture.GestureRecognizer.GestureRecognizerListener;
import gesture.LGestureRecognizer;
import graphics.GraphicsWrapper;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JMenuBar;
import javax.swing.JPanel;

import playback.Playback;
//import javax.swing.SwingUtilities;

import models.Drum;
import models.Piano;
import models.PianoFactory;
import tracking.Track;
import views.Button;
import views.Button.OnClickListener;
import views.DrumView;
import views.MusicalSurfaceView;
import views.PianoView;
import views.TrackView;
import controllers.DrumController;
import controllers.InstrumentController;
import controllers.MusicalSurfaceController;
import controllers.PianoController;
import controllers.TrackController;



// This stores a polygonal line, creating by a stroke of the user's finger or pen.
class Stroke {
	// the points that make up the stroke, in world space coordinates
	private ArrayList< Point2D > points = new ArrayList< Point2D >();

	private float color_red = 0;
	private float color_green = 0;
	private float color_blue = 0;

	private float thickness = 5.0f;

	private AlignedRectangle2D boundingRectangle = new AlignedRectangle2D();
	private boolean isBoundingRectangleDirty = false;

	public void addPoint( Point2D p ) {
		points.add( p );
		isBoundingRectangleDirty = true;
	}
	public ArrayList< Point2D > getPoints() {
		return points;
	}

	public void setColor( float r, float g, float b ) {
		color_red = r;
		color_green = g;
		color_blue = b;
	}

	public float[] getColor(){
		return new float[] {color_red, color_green, color_blue};
	}

	public AlignedRectangle2D getBoundingRectangle() {
		if ( isBoundingRectangleDirty ) {
			boundingRectangle.clear();
			for ( Point2D p : points ) {
				boundingRectangle.bound( p );
			}
			isBoundingRectangleDirty = false;
		}
		return boundingRectangle;
	}
	public void markBoundingRectangleDirty() {
		isBoundingRectangleDirty = true;
	}

	public boolean isContainedInRectangle( AlignedRectangle2D r ) {
		return r.contains( getBoundingRectangle() );
	}
	public boolean isContainedInLassoPolygon( ArrayList< Point2D > polygonPoints ) {
		for ( Point2D p : points ) {
			if ( ! Point2DUtil.isPointInsidePolygon( polygonPoints, p ) )
				return false;
		}
		return true;
	}

	public void setThickness(float thickness)
	{
		this.thickness = thickness;
	}

	public void draw( GraphicsWrapper gw ) {
		gw.setColor( color_red, color_green, color_blue );
		gw.setLineWidth(thickness);
		gw.drawPolyline( points );
	}
}


// This stores a set of strokes.
// Even if there are multiple users interacting with the window at the same time,
// they all interect with a single instance of this class.
class Drawing {

	public ArrayList< Stroke > strokes = new ArrayList< Stroke >();

	private AlignedRectangle2D boundingRectangle = new AlignedRectangle2D();
	private boolean isBoundingRectangleDirty = false;

	public void addStroke( Stroke s ) {
		strokes.add( s );
		Memento.save(strokes);
		isBoundingRectangleDirty = true;
	}

	public AlignedRectangle2D getBoundingRectangle() {
		if ( isBoundingRectangleDirty ) {
			boundingRectangle.clear();
			for ( Stroke s : strokes ) {
				boundingRectangle.bound( s.getBoundingRectangle() );
			}
			isBoundingRectangleDirty = false;
		}
		return boundingRectangle;
	}
	public void markBoundingRectangleDirty() {
		isBoundingRectangleDirty = true;
	}

	public void draw( GraphicsWrapper gw ) {
		gw.setLineWidth( 5 );
		strokes = Memento.load();

		for ( Stroke s : strokes ) {
			s.draw( gw );
		}
		gw.setLineWidth( 1 );
	}

}

class Memento {

	static public ArrayList<ArrayList< Stroke >> mem = new ArrayList<ArrayList< Stroke >>();
	static int current = -1;

	static int getLast(){
		return mem.size()-1;
	}

	static ArrayList< Stroke > load(){
		//Initializing with blank canvas
		if(mem.size()==0){
			mem.add(new ArrayList< Stroke >());
			current++;
		}
		return new ArrayList< Stroke >(mem.get(current));
	}

	static void debug(){
		System.out.println("Number of lines : " + mem.get(current).size());
		System.out.println("Current : " + current);
		System.out.println("Last : " + getLast());
	}

	static void save(ArrayList< Stroke > strokes){
		//If not at latest state, delete the ones after
		int diff = getLast()-current;
		if(diff!=0){
			for(int i=0; i<diff;i++){
				mem.remove(current+1);
			}
		}
		mem.add(new ArrayList< Stroke >(strokes));
		current++;
	}

	static void undo(){
		if(canUndo()){
			current--;
			//debug();
		}
	}

	static void redo(){
		if(canRedo()){
			current++;
			//debug();
		}
	}

	static boolean canUndo(){
		return current>0;
	}

	static boolean canRedo(){
		return current<getLast();
	}
}

// This class stores the current position of a finger,
// as well as the history of previous positions of that finger
// during its drag.
//
// An instance of this class is created when a finger makes contact
// with the multitouch surface.  The instance stores all
// subsequent positions of the finger, and is destroyed
// when the finger is lifted off the multitouch surface.
class MyCursor {

	// Each finger in contact with the multitouch surface is given
	// a unique id by the framework (or computing platform).
	// There is no guarantee that these ids will be consecutive nor increasing.
	// For example, when two fingers are in contact with the multitouch surface,
	// their ids may be 0 and 1, respectively,
	// or their ids may be 14 and 9, respectively.
	public int id; // identifier

	// This stores the history of positions of the "cursor" (finger)
	// in pixel coordinates.
	// The first position is where the finger pressed down,
	// and the last position is the current position of the finger.
	private ArrayList< Point2D > positions = new ArrayList< Point2D >();

	private float totalDistanceAlongDrag = 0;
	private float distanceFromStartToEndOfDrag = 0;


	// These are used to store what the cursor is being used for.
	public static final int TYPE_NOTHING = 0; // in this case, the cursor is ignored
	public static final int TYPE_INTERACTING_WITH_WIDGET = 1; // interacting with a virtual button, menu, etc.
	public static final int TYPE_INKING = 2; // creating a stroke
	public static final int TYPE_CAMERA_PAN_ZOOM = 3;
	public static final int TYPE_SELECTION = 4;
	public static final int TYPE_DIRECT_MANIPULATION = 5;
	public static final int TYPE_MULTI_SELECTION = 6;
	public int type = TYPE_NOTHING;


	// This is only used if (type == TYPE_INTERACTING_WITH_WIDGET),
	// in which case it stores the index of the palette button under the cursor.
	public int indexOfButton = 0;


	public MyCursor( int id, float x, float y ) {
		this.id = id;
		positions.add( new Point2D(x,y) );
	}

	public ArrayList< Point2D > getPositions() { return positions; }

	public void addPosition( Point2D p ) {
		if ( positions.size() >= 1 ) {
			totalDistanceAlongDrag += p.distance( positions.get(positions.size()-1) );
			distanceFromStartToEndOfDrag = p.distance( positions.get(0) );
		}
		positions.add( p );
	}

	public Point2D getFirstPosition() {
		if ( positions == null || positions.size() < 1 )
			return null;
		return positions.get( 0 );
	}
	public Point2D getCurrentPosition() {
		if ( positions == null || positions.size() < 1 )
			return null;
		return positions.get( positions.size()-1 );
	}
	public Point2D getPreviousPosition() {
		if ( positions == null || positions.size() == 0 )
			return null;
		if ( positions.size() == 1 )
			return positions.get( 0 );
		return positions.get( positions.size()-2 );
	}

	public boolean doesDragLookLikeLassoGesture() {
		return totalDistanceAlongDrag / (float)distanceFromStartToEndOfDrag > 2.5f;
	}

	public int getType() { return type; }
	public void setType( int type ) { this.type = type; }
	public void setType(
			int type,
			int indexOfButton // only used if (type == TYPE_INTERACTING_WITH_WIDGET)
			) {
		this.type = type;
		this.indexOfButton = indexOfButton;
	}
}


//This stores a set of instances of MyCursor.
//Each cursor can be identified by its id,
//which is assigned by the framework or computing platform.
//Each cursor can also be identified by its index in this class's container.
//For example, if an instance of this class is storing 3 cursors,
//their ids may be 2, 18, 7,
//but their indices should be 0, 1, 2.
class CursorContainer {
	private ArrayList< MyCursor > cursors = new ArrayList< MyCursor >();
	private ArrayList< MyCursor > unusedCursors = new ArrayList< MyCursor >();

	public int getNumCursors() { return cursors.size(); }
	public MyCursor getCursorByIndex( int index ) { return cursors.get( index ); }

	public int getNumUnusedCursors() { return unusedCursors.size(); }
	public MyCursor getUnusedCursorByIndex( int index ) { return unusedCursors.get( index ); }

	public int findIndexOfCursorById( int id ) {
		for ( int i = 0; i < cursors.size(); ++i ) {
			if ( cursors.get(i).id == id )
				return i;
		}
		return -1;
	}

	public int findIndexOfUnusedCursorById( int id ) {
		for ( int i = 0; i < unusedCursors.size(); ++i ) {
			if ( unusedCursors.get(i).id == id )
				return i;
		}
		return -1;
	}

	public MyCursor getCursorById( int id ) {
		int index = findIndexOfCursorById( id );
		return ( index == -1 ) ? null : cursors.get( index );
	}

	// Returns the number of cursors that are of the given type.
	public int getNumCursorsOfGivenType( int type ) {
		int num = 0;
		for ( int i = 0; i < cursors.size(); ++i ) {
			if ( cursors.get(i).getType() == type )
				num ++;
		}
		return num;
	}

	// Returns the (i)th cursor of the given type,
	// or null if no such cursor exists.
	// Can be used for retrieving both cursors of type TYPE_CAMERA_PAN_ZOOM, for example,
	// by calling getCursorByType( MyCursor.TYPE_CAMERA_PAN_ZOOM, 0 )
	// and getCursorByType( MyCursor.TYPE_CAMERA_PAN_ZOOM, 1 ),
	// when there may be cursors of other type present at the same time.
	public MyCursor getCursorByType( int type, int i ) {
		for ( int ii = 0; ii < cursors.size(); ++ii ) {
			if ( cursors.get(ii).getType() == type ) {
				if ( i == 0 )
					return cursors.get(ii);
				else
					i --;
			}
		}
		return null;
	}

	// Returns index of updated cursor.
	// If a cursor with the given id does not already exist, a new cursor for it is created.
	public int updateCursorById(
			int id,
			float x, float y
			) {
		Point2D updatedPosition = new Point2D( x, y );
		int index = findIndexOfCursorById( id );
		if ( index == -1 ) {
			MyCursor cursor = new MyCursor( id, x, y );
			cursors.add(cursor);
			unusedCursors.add(cursor);
			index = cursors.size() - 1;
		}
		MyCursor c = cursors.get( index );
		if ( ! c.getCurrentPosition().equals( updatedPosition ) ) {
			c.addPosition( updatedPosition );
		}
		return index;
	}
	public void removeCursorByIndex( int index ) {
		cursors.remove( index );
	}

	public void removeUnusedCursorByIndex( int index ) {
		unusedCursors.remove( index );
	}

	public ArrayList< Point2D > getWorldPositionsOfCursors( GraphicsWrapper gw ) {
		ArrayList< Point2D > positions = new ArrayList< Point2D >();
		for ( MyCursor cursor : cursors ) {
			positions.add( gw.convertPixelsToWorldSpaceUnits( cursor.getCurrentPosition() ) );
		}
		return positions;
	}
}

class PaletteButton {
	public static final int width = Constant.BUTTON_WIDTH; // in pixels
	public static final int height = Constant.BUTTON_HEIGHT; // in pixels
	public int x0, y0; // coordinates of upper left corner of button, in pixels, with respect to the upper left corner of the palette that contains us
	String label = "";
	String tooltip = "";

	public boolean isShown = true;

	public boolean isPressed = false; // if true, the button is drawn differently
	public boolean isSticky = false; // if true, the button remains pressed after the finger has lifted off (useful for modal or radio buttons)

	public PaletteButton( int x0, int y0, String label, String tooltip, boolean isSticky ) {
		this(x0, y0, label, tooltip, isSticky, true);
	}

	public PaletteButton( int x0, int y0, String label, String tooltip, boolean isSticky, boolean isShown ) {
		this.x0 = x0;
		this.y0 = y0;
		this.label = label;
		this.tooltip = tooltip;
		this.isSticky = isSticky;
		this.isShown = isShown;
	}

	// returns bounding box in the local space of the palette
	public AlignedRectangle2D getBoundingRectangle() {
		if (!isShown)
		{
			return new AlignedRectangle2D( new Point2D(0,0), new Point2D(0,0) );
		}
		return new AlignedRectangle2D( new Point2D(x0,y0), new Point2D(x0+width,y0+height) );
	}

	public boolean contains(
			float x, float y // pixel coordinates in the local space of the palette
			) {
		return getBoundingRectangle().contains( new Point2D(x,y) );
	}

	public void draw(
			int palette_x, int palette_y, // upper left corner of the palette that contains us, in pixels
			GraphicsWrapper gw
			) {
		if (isShown)
		{
			// draw background
			if ( isPressed ) {
				gw.setColor( 0, 0, 0, Palette.alpha );
				gw.fillRect( palette_x + x0, palette_y + y0, width, height );
				// set the foreground color in preparation for drawing the label
				gw.setColor( 1, 1, 1 );
			}
			else {
				gw.setColor( 1, 1, 1, Palette.alpha );
				gw.fillRect( palette_x + x0, palette_y + y0, width, height );
				// draw border
				gw.setColor( 0, 0, 0 );
				gw.drawRect( palette_x + x0, palette_y + y0, width, height );
			}
			// draw text label
			int stringWidth = Math.round( gw.stringWidth( label ) );
			gw.drawString( palette_x + x0+(width-stringWidth)/2, palette_y + y0+height/2+Constant.TEXT_HEIGHT/2, label );			
		}
	}
}

class Palette {
	public int width, height; // in pixels
	public int x0, y0; // in pixels
	public static final float alpha = 0.3f; // if between 0 and 1, the palette is drawn semi-transparent

	public ArrayList< PaletteButton > buttons = null;

	// These variables are initialized in the contructor,
	// to save the index of each button,
	// but after that they should never change.
	public int movePalette_buttonIndex;
	public int ink_buttonIndex;
	public int select_buttonIndex;
	public int manipulate_buttonIndex;
	public int camera_buttonIndex;
	public int undo_buttonIndex;

	public int black_buttonIndex;
	public int red_buttonIndex;
	public int green_buttonIndex;
	public int horizFlip_buttonIndex;
	public int frameAll_buttonIndex;
	public int redo_buttonIndex;

	public int multiSelect_buttonIndex;

	public float currentThickness = 5.0f;
	public int medium_buttonIndex;
	public int thin_buttonIndex;
	public int thick_buttonIndex;

	public int hide_buttonIndex;
	public int show_buttonIndex;

	public int currentlyActiveModalButton; // could be equal to any of ink_buttonIndex, select_buttonIndex, manipulate_buttonIndex, camera_buttonIndex

	public int currentlyActiveColorButton; // could be equal to any of black_buttonIndex, red_buttonIndex, green_buttonIndex
	public float current_red = 0;
	public float current_green = 0;
	public float current_blue = 0;

	//	public float customRed = 0.0f;
	//	public float customGreen = 0.0f;
	//	public float customBlue = 0.0f;

	public Palette() {
		final int W = PaletteButton.width;
		final int H = PaletteButton.height;
		PaletteButton b = null;
		buttons = new ArrayList< PaletteButton >();


		// Create first row of buttons

		b = new PaletteButton(0, 0, "-", "Hide most palette buttons.", true);
		hide_buttonIndex = buttons.size();
		buttons.add(b);

		b = new PaletteButton(   W, 0, "Move", "Drag on this button to move the palette.", false );
		movePalette_buttonIndex = buttons.size();
		buttons.add( b );

		b = new PaletteButton(   2*W, 0, "Ink", "When active, use other fingers to draw ink strokes.", true );
		ink_buttonIndex = buttons.size();
		buttons.add( b );

		b = new PaletteButton( 3*W, 0, "Select", "When active, use another finger to select strokes.", true );
		select_buttonIndex = buttons.size();
		buttons.add( b );

		b = new PaletteButton( 4*W, 0, "Manip.", "When active, use one or two other fingers to directly manipulate the selection.", true );
		manipulate_buttonIndex = buttons.size();
		buttons.add( b );

		b = new PaletteButton( 5*W, 0, "Camera", "When active, use one or two other fingers to directly manipulate the camera.", true );
		camera_buttonIndex = buttons.size();
		buttons.add( b );

		b = new PaletteButton(6*W, 0, "Thin", "Draw thin lines.", true );
		thin_buttonIndex = buttons.size();
		buttons.add(b);

		b = new PaletteButton(7 * W, 0, "Regular", "Draw regular lines.", true );
		medium_buttonIndex = buttons.size();
		buttons.add(b);

		b = new PaletteButton( 8*W, 0, "Undo", "Go back to last modification.", false );
		undo_buttonIndex = buttons.size();
		buttons.add( b );

		// Create second row of buttons

		b = new PaletteButton(0, H, "+", "Show all palette buttons.", true);
		show_buttonIndex = buttons.size();
		buttons.add(b);

		b = new PaletteButton(   W, H, "Black", "Changes ink color.", true );
		black_buttonIndex = buttons.size();
		buttons.add( b );

		b = new PaletteButton(   2*W, H, "Red", "Changes ink color.", true );
		red_buttonIndex = buttons.size();
		buttons.add( b );

		b = new PaletteButton( 3*W, H, "Green", "Changes ink color.", true );
		green_buttonIndex = buttons.size();
		buttons.add( b );

		b = new PaletteButton( 4*W, H, "Hor. Flip", "Flip the selection horizontally (around a vertical axis).", false );
		horizFlip_buttonIndex = buttons.size();
		buttons.add( b );

		b = new PaletteButton( 5*W, H, "Frame all", "Frames the entire drawing.", false );
		frameAll_buttonIndex = buttons.size();
		buttons.add( b );

		b = new PaletteButton(6 * W, H, "Thick", "Draw thick lines.", true);
		thick_buttonIndex = buttons.size();
		buttons.add(b);

		b = new PaletteButton(7 * W, H, "Mult. Sel.", "When active, use another finger to select strokes.", true);
		multiSelect_buttonIndex = buttons.size();
		buttons.add(b);

		b = new PaletteButton( 8*W, H, "Redo", "Go back to next modification.", false );
		redo_buttonIndex = buttons.size();
		buttons.add( b );

		// Initialize remaining state

		buttons.get( ink_buttonIndex ).isPressed = true;
		currentlyActiveModalButton = ink_buttonIndex;
		buttons.get( black_buttonIndex ).isPressed = true;
		currentlyActiveColorButton = black_buttonIndex;
		current_red = current_green = current_blue = 0;

		buttons.get(medium_buttonIndex).isPressed = true;
		buttons.get(show_buttonIndex).isPressed = true;

		resize();
	}

	public void resize()
	{
		// Figure out the width and height of the palette.
		// To do this, compute a bounding rectangle.
		AlignedRectangle2D boundingRectangle = new AlignedRectangle2D();
		for ( int j = 0; j < buttons.size(); ++j ) {
			if (buttons.get(j).isShown)
			{
				boundingRectangle.bound( buttons.get(j).getBoundingRectangle() );				
			}
		}
		// Note that the bounding rectangle contains coordinates in the palette's local space.
		// We only store the width and height of the bounding rectangle.
		width = Math.round( boundingRectangle.getDiagonal().x() );
		height = Math.round( boundingRectangle.getDiagonal().y() );
	}
	public AlignedRectangle2D getBoundingRectangle() {
		return new AlignedRectangle2D( new Point2D(x0,y0), new Point2D(x0+width,y0+height) );
	}
	public Point2D getCenter() {
		return getBoundingRectangle().getCenter();
	}
	public boolean contains( float x, float y ) {
		return getBoundingRectangle().contains( new Point2D(x,y) );
	}
	// returns -1 if no button contains the given point
	public int indexOfButtonContainingTheGivenPoint( float x, float y ) {
		for ( int j = 0; j < buttons.size(); ++j ) {
			PaletteButton b = buttons.get( j );
			if ( b.contains( x-x0, y-y0 ) ) // the subtraction converts the coordinates to the palette's local coordinate system
				return j;
		}
		return -1;
	}

	public void draw( GraphicsWrapper gw ) {
		// draw border
		gw.setColor( 0, 0, 0 );
		gw.drawRect( x0, y0, width, height );

		for ( PaletteButton b : buttons ) {
			b.draw( x0, y0, gw );
		}
	}
}

public class SimpleWhiteboard implements Runnable, ActionListener, GestureRecognizerListener {

	public MultitouchFramework multitouchFramework = null;
	public GraphicsWrapper gw = null;
	JButton frameAllButton;
	JButton testButton1;
	JButton testButton2;

	views.Button btnRecord= new Button("RECORD", 10, 10);
	views.Button btnStop = 	new Button("STOP", 10, 10);
	//views.Button btnPlay = 	new Button("PLAY", 170, 10);
	
	Thread thread = null;
	boolean threadSuspended;

	int mouse_x, mouse_y;

	Drawing drawing = new Drawing();

	List<InstrumentController> instruments = new ArrayList<InstrumentController>();
	List<InstrumentController> instrumentsToRemove = new ArrayList<InstrumentController>();
	List<TrackController> tracks = new ArrayList<TrackController>();
	List<GestureRecognizer> gestureRecognizers = new ArrayList<GestureRecognizer>();
	List<CursorLink> cursorLinks = new ArrayList<CursorLink>();

	private CursorContainer cursorContainer = new CursorContainer();
	
	public SimpleWhiteboard( MultitouchFramework mf, GraphicsWrapper gw ) {
		multitouchFramework = mf;
		this.gw = gw;
		multitouchFramework.setPreferredWindowSize(Constant.INITIAL_WINDOW_WIDTH,Constant.INITIAL_WINDOW_HEIGHT);

		gw.setFontHeight( Constant.TEXT_HEIGHT );

		gw.frame( new AlignedRectangle2D( new Point2D(-100,-100), new Point2D(100,100) ), true );

		btnRecord.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(Button button) {
				if(Playback.getInstance().isRecording){
					Track t = Playback.getInstance().stopRecording();
					if (!t.cues.isEmpty()) {
						tracks.add(new TrackController(t, new TrackView(t, 40, 80*(tracks.size()+1))));
					}
					button.text="Record";
				}
				else{
					Playback.getInstance().startRecording();
					button.text="Stop";
				}
			}
		});
		
		btnStop.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(Button button) {
				Track t = Playback.getInstance().stopRecording();
				if (!t.cues.isEmpty()) {
					tracks.add(new TrackController(t, new TrackView(t, 40, 80*(tracks.size()+1))));					
				}
			}
		});
		
		init();
	}

	private void init(){
		//		addTestPiano();

		initGestureRecognizers();
	}

	private void initGestureRecognizers(){
		/*
		 * Add your gesture recognizers here
		 */
		GestureRecognizer gr = new LGestureRecognizer();
		gestureRecognizers.add(gr);
		gr = new DiagonalGestureRecognizer();
		gestureRecognizers.add(gr);

		for(GestureRecognizer g : gestureRecognizers){
			g.setListener(this);
		}
	}

	@Override
	public void didRecognizeGesture(String gesture, GestureData data) {
		if("LGesture".equals(gesture)){
			int keysCount = 2*(int) (data.width / PianoView.KEY_WIDTH);
			if(keysCount < 4){
				keysCount = 4;
			}
			System.out.println("KEYS = " + keysCount);
			Piano piano = PianoFactory.buildPianoWithKeysCount(keysCount); 
			PianoView pianoView = new PianoView(piano,
					data.startx,
					data.starty,
					data.height > 100 ? data.height : 100);
			pianoView.angle = data.angle;
			final PianoController pianoController = new PianoController(piano, pianoView);
			instruments.add(pianoController);
			pianoView.btnDelete.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(Button button) {
					instrumentsToRemove.add(pianoController);
				}
			});
		}else{
			if("DiagonalGesture".equals(gesture)){
				MusicalSurfaceView musicalSurfaceView = new MusicalSurfaceView(data.startx,data.starty,data.width,data.height);
				final MusicalSurfaceController musicalSurfaceController = new MusicalSurfaceController(musicalSurfaceView);
				musicalSurfaceView.btnDelete.setOnClickListener(new OnClickListener(){
					@Override
					public void onClick(Button button) {
						instrumentsToRemove.add(musicalSurfaceController);
					}});
				instruments.add(musicalSurfaceController);
			}
		}
	}

	public JMenuBar createMenuBar(){
		return null;
	}

	public JPanel createPanelOfWidgets(){
		return null;
	}

	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		if ( source == frameAllButton ) {
			gw.frame( drawing.getBoundingRectangle(), true );
			multitouchFramework.requestRedraw();
		}
	}

	// Called by the framework at startup time.
	public void startBackgroundWork() {
		if ( thread == null ) {
			thread = new Thread( this );
			threadSuspended = false;
			thread.start();
		}
		else {
			if ( threadSuspended ) {
				threadSuspended = false;
				synchronized( this ) {
					notify();
				}
			}
		}
	}
	public void stopBackgroundWork() {
		threadSuspended = true;
	}
	public void run() {
		try {
			int sleepIntervalInMilliseconds = 100;
			while (true) {

				synchronized( this ) {
					for(InstrumentController ic : instrumentsToRemove){
						instruments.remove(ic);
					}
					instrumentsToRemove.clear();
				}
				multitouchFramework.requestRedraw();

				// Now the thread checks to see if it should suspend itself
				if ( threadSuspended ) {
					synchronized( this ) {
						while ( threadSuspended ) {
							wait();
						}
					}
				}
				thread.sleep( sleepIntervalInMilliseconds );  // interval given in milliseconds
			}
		}
		catch (InterruptedException e) { }
	}

	public synchronized void draw() {
		gw.clear(1,1,1);
		gw.setColor(0,0,0);
		gw.setupForDrawing();

		gw.setCoordinateSystemToWorldSpaceUnits();
		gw.enableAlphaBlending();

		drawing.draw( gw );

		gw.setCoordinateSystemToPixels();
		
		btnRecord.draw(gw);
		/*
		if(!Playback.getInstance().isRecording){
			btnRecord.draw(gw);
		}else{
			btnStop.draw(gw);
		}*/
		
		//TODO: code de merde, move ca � l'ext�rieur du draw
		for(CursorLink cl : cursorLinks){

			MyCursor cursor01 = cursorContainer.getCursorById(cl.cursorId1);
			MyCursor cursor02 = cursorContainer.getCursorById(cl.cursorId2);
			
			if(cursor01 != null && cursor02 != null){
				float DrumPositionX = (cursor01.getCurrentPosition().x()+cursor02.getCurrentPosition().x())/2;
				float DrumPositionY = (cursor01.getCurrentPosition().y()+cursor02.getCurrentPosition().y())/2;
				float DrumRadius = cursor01.getCurrentPosition().distance( cursor02.getCurrentPosition() )/2;

				Drum drum = new Drum();
				DrumView drumView = new DrumView(drum, DrumPositionX-DrumRadius, DrumPositionY-DrumRadius, DrumRadius);

				drumView.draw(gw);
			}
		}
		for(InstrumentController ic : instruments){
			ic.getView().draw(gw);
		}
		
		for (TrackController tc : tracks)
		{
			tc.getView().draw(gw);
		}
	}

	public synchronized void keyPressed( KeyEvent e ) {
		System.out.println("Key '" + e.getKeyChar() + "' pressed!");
	}
	public synchronized void keyReleased( KeyEvent e ) {
	}
	public synchronized void keyTyped( KeyEvent e ) {
		System.out.println("Key '" + e.getKeyChar() + "' typed!");
	}
	public synchronized void mouseEntered( MouseEvent e ) {
	}
	public synchronized void mouseExited( MouseEvent e ) {
	}
	public synchronized void mouseClicked( MouseEvent e ) {
	}

	public synchronized void mousePressed(MouseEvent e) {
		mouse_x = e.getX();
		mouse_y = e.getY();

		// multitouchFramework.requestRedraw();
	}

	public synchronized void mouseReleased(MouseEvent e) {
		mouse_x = e.getX();
		mouse_y = e.getY();

		// multitouchFramework.requestRedraw();
	}

	public synchronized void mouseDragged(MouseEvent e) {

		mouse_x = e.getX();
		mouse_y = e.getY();

		// multitouchFramework.requestRedraw();
	}

	public synchronized void mouseMoved(MouseEvent e) {
		mouse_x = e.getX();
		mouse_y = e.getY();

		// multitouchFramework.requestRedraw();
	}

	public synchronized void processMultitouchInputEvent( int id, float x, float y, int type ){ 
		boolean isPlayingInstrument = false;
		boolean isMovingInstrument = false;

		btnRecord.onTouchEvent(id, x, y, type);
		btnStop.onTouchEvent(id, x, y, type);
		
		for (TrackController tc : tracks)
		{
			tc.getView().onTouchEvent(id, x, y, type);
		}
		
		for(InstrumentController ic : instruments){
			isPlayingInstrument = ic.getView().onTouchEvent(id, x, y, type);
			isMovingInstrument =  ic.getView().isPressingButton(x,y);
			if(isPlayingInstrument || isMovingInstrument){
				break;
			}
		}
		
		
		boolean isCreatingDrum = false;
		//pas faire le ontouch du gesture si c'est la creation du drum
		for(CursorLink cl : cursorLinks){
			if(cl.contains(id)){
				isCreatingDrum = true;
			}
		}

		if(!isCreatingDrum && !isMovingInstrument){
			for(GestureRecognizer gr : gestureRecognizers){
				gr.onTouchEvent(id, x, y, type);
			}
		}

		//Comparer la distance avec les autres cursors s'ils ne sont pas sur un instrument
		//et s'ils ne sont pas en train de dessiner un autre instrument
		if(!isPlayingInstrument){
			Point2D currentCursor = new Point2D(x,y);
			Point2D compareCursor;
			float closestDistanceBetweenCursor = 300;

			for ( int j = 0; j < cursorContainer.getNumCursors(); ++j ) {
				compareCursor = new Point2D(cursorContainer.getCursorByIndex(j).getCurrentPosition().x(), cursorContainer.getCursorByIndex(j).getCurrentPosition().y());
				if(closestDistanceBetweenCursor < currentCursor.distance(compareCursor)){
					closestDistanceBetweenCursor = currentCursor.distance(compareCursor);
				}
			}


			cursorContainer.updateCursorById(id, x, y);
			int cursorIndex = cursorContainer.findIndexOfCursorById( id );
			int unusedCursorIndex = cursorContainer.findIndexOfUnusedCursorById( id );

			if(cursorContainer.getNumUnusedCursors() >= 2){
				MyCursor newestCursor = cursorContainer.getCursorByIndex(cursorContainer.getNumCursors()-1);

				float defaultDistance = 300;
				float cursorDistance = defaultDistance;
				MyCursor closestCursor = null;
				int index = 0;

				// comparer avec les autres cursors
				for(int k=0; k < cursorContainer.getNumUnusedCursors()-1; k++){
					MyCursor cursor = cursorContainer.getUnusedCursorByIndex(k);
					float currentDistance = newestCursor.getCurrentPosition().distance( cursor.getCurrentPosition() );

					if(currentDistance < cursorDistance){
						cursorDistance = currentDistance;
						closestCursor = cursor;
						index = k;
					}

				}

				if(cursorDistance < defaultDistance){

					cursorContainer.removeUnusedCursorByIndex(cursorContainer.getNumUnusedCursors()-1);
					cursorContainer.removeUnusedCursorByIndex(index);

					CursorLink cl = new CursorLink(newestCursor.id, closestCursor.id);
					cursorLinks.add(cl);

				}


			}

			if ( type == MultitouchFramework.TOUCH_EVENT_UP ){
				CursorLink clToRemove = null;
				for(CursorLink cl : cursorLinks){
					if(cl.contains(id)){
						clToRemove = cl;
					}
				}

				if(clToRemove != null){
					MyCursor cursor01 = cursorContainer.getCursorById(clToRemove.cursorId1);
					MyCursor cursor02 = cursorContainer.getCursorById(clToRemove.cursorId2);

					float DrumPositionX = (cursor01.getCurrentPosition().x()+cursor02.getCurrentPosition().x())/2;
					float DrumPositionY = (cursor01.getCurrentPosition().y()+cursor02.getCurrentPosition().y())/2;
					float DrumRadius = cursor01.getCurrentPosition().distance( cursor02.getCurrentPosition() )/2;

					Drum drum = new Drum();
					DrumView drumView = new DrumView(drum, DrumPositionX-DrumRadius, DrumPositionY-DrumRadius, DrumRadius);
					final DrumController drumController = new DrumController(drum, drumView);
					drumView.btnDelete.setOnClickListener(new OnClickListener(){

						@Override
						public void onClick(Button button) {
							instrumentsToRemove.add(drumController);
						}});
					instruments.add(drumController);

					cursorLinks.remove(clToRemove);		

				}

				if(unusedCursorIndex != -1){
					cursorContainer.removeUnusedCursorByIndex(unusedCursorIndex);
				}

				cursorContainer.removeCursorByIndex( cursorIndex );

			}
		} else{

			cursorContainer.updateCursorById(id, x, y);
			int cursorIndex = cursorContainer.findIndexOfCursorById( id );
			int unusedCursorIndex = cursorContainer.findIndexOfUnusedCursorById( id );

			if(type == MultitouchFramework.TOUCH_EVENT_DOWN){
				cursorContainer.removeUnusedCursorByIndex(unusedCursorIndex);
			}

			if ( type == MultitouchFramework.TOUCH_EVENT_UP ){

				cursorContainer.removeCursorByIndex( cursorIndex );


			}
		}
		multitouchFramework.requestRedraw();
	}
}
