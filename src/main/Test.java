package main;
import models.Piano;
import models.PianoFactory;
import views.PianoViewTest;
import controllers.PianoController;


public class Test {

	public static void main(String[] args) {

		Piano piano = PianoFactory.buildTestPiano(); 
		PianoViewTest pianoView = new PianoViewTest(piano);
		PianoController pianoController = new PianoController(piano, pianoView);
		
		pianoView.show();
	}
}
