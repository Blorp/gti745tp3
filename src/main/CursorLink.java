package main;

public class CursorLink {
	
	public int cursorId1;
	public int cursorId2;
	
	public CursorLink(int cursorId1, int cursorId2){
		this.cursorId1 = cursorId1;
		this.cursorId2 = cursorId2;
	}

	public boolean contains(int id){
		if(id == cursorId1 || id == cursorId2){
			return true;
		}else{
			return false;
		}
	}
	
}
