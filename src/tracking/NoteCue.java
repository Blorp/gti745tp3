package tracking;

import playback.Note;
import playback.Playback;

public class NoteCue extends Cue {

	public Note note;
	public long duration;
	
	Playback playback;
	
	public NoteCue(Note note, long duration, long startTime){
		super(startTime);
		this.note=note;
		this.duration=duration;
		this.playback = Playback.getInstance();
	}
	
	public void setDurationForLastCueOfNote(long endTime){
		this.duration = endTime - this.startTime;
	}
	
	public void play(){
		try {
			Thread.sleep(startTime);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		playback.play(note, duration);
	}
}
