package tracking;

import java.util.ArrayList;

import playback.Note;

public class Track {

	int loopCount=1;
	public long duration;
	
	public TrackListener listener;
	
	public ArrayList<Cue> cues;
	
	public Track(long duration, int loopCount){
		this.duration=duration;
		this.loopCount=loopCount;
		cues = new ArrayList<Cue>();
	}
	
	public Track(long duration){
		this.duration=duration;
		this.loopCount=1;
		cues = new ArrayList<Cue>();
	}
	
	public void play(){
		System.out.println(cues.size());
		for(int i = loopCount; i>0; i--){
			for(final Cue cue : cues){
				Runnable r = new Runnable(){
					@Override
					public void run() {
						cue.play();
						if(cue instanceof NoteCue){
							NoteCue noteCue = (NoteCue)cue;
							if(listener != null){
								listener.onNoteCuePlayed(noteCue.note);
							}
						}
					}
				};
				new Thread(r).start();
			}
		}
	}
	
	public void add(Cue cue){
		cues.add(cue);
	}
	
	public void removeTrackCueFromTrack(Track track){
		TrackCue tc;
		Cue cue;
		for(int i=0; i<this.cues.size();i++){
			cue=this.cues.get(i);
			if(cue.getClass().equals(TrackCue.class)){
				tc = (TrackCue) cue;
				if(tc.track.equals(track)){
					this.cues.remove(i);
					break;
				}
			}
		}
	}
	
	public interface TrackListener{
		void onNoteCuePlayed(Note note);
	}
}
