package tracking;

public abstract class Cue {

	long startTime;
	
	public Cue(long startTime){
		this.startTime=startTime;
	}
	
	abstract public void play();
}
