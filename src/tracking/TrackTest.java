package tracking;

import playback.Note;

public class TrackTest {
	public static void main(String[] args) {
		Track t1 = new Track(6000);
		Track t2 = new Track(2000);
		Track t3 = new Track(12000);
		t1.add(new NoteCue(Note.get("D3"), 500, 0));
		t1.add(new NoteCue(Note.get("F3"), 500, 500));
		t1.add(new NoteCue(Note.get("A3"), 500, 500));
		t1.add(new NoteCue(Note.get("F3"), 500, 1000));
		t1.add(new NoteCue(Note.get("A3"), 500, 1000));
		t1.add(new NoteCue(Note.get("E3"), 500, 1500));
		t1.add(new NoteCue(Note.get("G3"), 1000, 2000));
		t1.add(new NoteCue(Note.get("B3"), 1000, 2000));
		t1.add(new NoteCue(Note.get("F3"), 500, 3000));
		t1.add(new NoteCue(Note.get("A3"), 500, 3500));
		t1.add(new NoteCue(Note.get("C4"), 500, 3500));
		t1.add(new NoteCue(Note.get("A3"), 500, 4000));
		t1.add(new NoteCue(Note.get("C4"), 500, 4000));
		t1.add(new NoteCue(Note.get("E3"), 500, 4500));
		t1.add(new NoteCue(Note.get("G3"), 1000, 5000));
		t1.add(new NoteCue(Note.get("B3"), 1000, 5000));
		
		
		t2.add(new NoteCue(Note.get("D4"), 300, 0));
		t2.add(new NoteCue(Note.get("F4"), 300, 300));
		t2.add(new NoteCue(Note.get("D5"), 900, 600));
		t2.add(new NoteCue(Note.get("D4"), 300, 1500));
		t2.add(new NoteCue(Note.get("F4"), 300, 1800));
		t2.add(new NoteCue(Note.get("D5"), 900, 2100));
		t2.add(new NoteCue(Note.get("E5"), 800, 3000));
		t2.add(new NoteCue(Note.get("F5"), 250, 3800));
		t2.add(new NoteCue(Note.get("E5"), 250, 4050));
		t2.add(new NoteCue(Note.get("F5"), 250, 4300));
		t2.add(new NoteCue(Note.get("E5"), 250, 4550));
		t2.add(new NoteCue(Note.get("C5"), 200, 4800));
		t2.add(new NoteCue(Note.get("A4"), 900, 5000));
		
		t3.add(new TrackCue(t1, 0));
		t3.add(new TrackCue(t2, 6050));
		t3.add(new TrackCue(t1, 6000));
		
		//t1.play();
		//t2.play();
		t3.play();
	}
}
