package tracking;

public class TrackCue extends Cue{

	public Track track;
	
	public TrackCue(Track track, long startTime){
		super(startTime);
		this.track=track;
	}
	
	public void play(){
		try {
			Thread.sleep(startTime);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		track.play();
	}
}
