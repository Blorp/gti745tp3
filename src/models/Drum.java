package models;

import java.util.ArrayList;
import java.util.List;

import playback.Note;

public class Drum {

	public List<Note> keys = new ArrayList<Note>();
	public Note currentKey;
	
	public Drum(){
		keys.add(Note.get("E1B", 9));//Hand Clap
		keys.add(Note.get("C1", 9));//Bass Drum 1 
		keys.add(Note.get("E2B", 9));//Ride Cymbal
		keys.add(Note.get("F2", 9));//Ride Bell
		keys.add(Note.get("F2#", 9));//Tambourine
	}

}
