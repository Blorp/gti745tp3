package models;

import playback.Note;
import playback.Playback;

public class PianoFactory {

	public static Piano buildTestPiano(){
		Piano p = new Piano();
		
		int c = Playback.getInstance().getPianoChannel();
		
		p.keys.add(Note.get("C4",c));
		p.keys.add(Note.get("C4#",c));
		p.keys.add(Note.get("D4",c));
		p.keys.add(Note.get("D4#",c));
		p.keys.add(Note.get("E4",c));		
		p.keys.add(Note.get("F4",c));
		p.keys.add(Note.get("F4#",c));
		p.keys.add(Note.get("G4",c));
		p.keys.add(Note.get("G4#",c));
		p.keys.add(Note.get("A4",c));
		p.keys.add(Note.get("A4#",c));
		p.keys.add(Note.get("B4",c));
		p.keys.add(Note.get("C5",c));
		
		return p;
	}
	
	public static Piano buildPianoWithKeysCount(int keysCount){
		Piano p = new Piano();
		
		int c = Playback.getInstance().getPianoChannel();
		
		int baseNote = 60;
		
		for(int i = 0; i< keysCount; i++){
			p.keys.add(new Note(baseNote+i,c));
		}
		
		return p;
	}
}
