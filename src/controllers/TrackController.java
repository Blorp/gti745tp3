package controllers;


import graphics.GraphicsWrapper;
import playback.Note;
import tracking.Track;
import tracking.TrackCue;
import views.TrackView;
import controllers.InstrumentController.InstrumentListener;


public class TrackController implements InstrumentListener{

	private Track track;
	private TrackView trackView;

	public TrackController(Track track, TrackView trackView){
		this.track = track;
		this.trackView = trackView;
	}
	
	@Override
	public void onStartPlayingNote(Note note) {
		
	}

	@Override
	public void onEndPlayingNote(Note note) {
		
	}
	
	public void play(){
		this.track.play();
	}
	
	public void addTrack(Track trackToAdd, long atTime){
		this.track.add(new TrackCue(trackToAdd, atTime));
	}
	
	//If time not specified, put at the end.
	public void addTrackToAnotherTrack(Track trackToAdd){
		this.track.add(new TrackCue(trackToAdd, this.track.duration));
	}
	
	public void removeTrackfromAnotherTrack(Track trackToRemove){
		this.track.removeTrackCueFromTrack(trackToRemove);
	}
	
	public TrackView getView()
	{
		return trackView;
	}

}
