package controllers;

import models.Drum;
import playback.Note;
import playback.Playback;
import views.DrumView;
import views.DrumView.DrumViewListener;
import views.View;

public class DrumController extends InstrumentController implements DrumViewListener{

	private Drum drum;
	private DrumView drumView;
	
	public DrumController(Drum drum, DrumView drumView){
		this.drum = drum;
		this.drumView = drumView;
		drumView.listener = this;
	}

	@Override
	public void onKeyPressed() {
		Playback.getInstance().play(drum.currentKey);		
	}

	@Override
	public void onKeyReleased() {
		Playback.getInstance().stop(drum.currentKey);	
	}
	
	@Override
	public View getView() {
		return drumView;
	}

}