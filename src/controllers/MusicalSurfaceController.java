package controllers;

import playback.Note;
import playback.Playback;
import views.MusicalSurfaceView;
import views.MusicalSurfaceView.MusicalSurfaceViewListener;
import views.View;

public class MusicalSurfaceController  extends InstrumentController implements MusicalSurfaceViewListener{

	private MusicalSurfaceView musicalSurfaceView;
	
	public MusicalSurfaceController(MusicalSurfaceView musicalSurfaceView){
		this.musicalSurfaceView = musicalSurfaceView;
		musicalSurfaceView.listener = this;
	}
	
	@Override
	public View getView() {
		return musicalSurfaceView;
	}

	@Override
	public void onKeyPressed(int number, int channel) {
		Playback.getInstance().play(Note.get(number, channel));	
	}

	@Override
	public void onKeyReleased(int number, int channel) {
		Playback.getInstance().stop(Note.get(number, channel));
	}

}
