package controllers;

import java.util.ArrayList;
import java.util.List;

import playback.Note;
import views.View;

public abstract class InstrumentController {

	private List<InstrumentListener> listeners = new ArrayList<InstrumentListener>();
	
	public void addListener(InstrumentListener l){
		listeners.add(l);
	}
	
	public void removeListener(InstrumentListener l){
		listeners.remove(l);
	}
	
	public interface InstrumentListener {
		void onStartPlayingNote(Note note);
		void onEndPlayingNote(Note note);
	}
	
	public abstract View getView();
}
