package controllers;

import models.Piano;
import playback.Note;
import playback.Playback;
import views.PianoView;
import views.PianoView.PianoViewListener;
import views.View;

public class PianoController extends InstrumentController implements PianoViewListener{

	private Piano piano;
	private PianoView pianoView;
	
	public PianoController(Piano piano, PianoView pianoView){
		this.piano = piano;
		this.pianoView = pianoView;
		pianoView.listener = this;
	}

	@Override
	public void onKeyPressed(Note note) {
		Playback.getInstance().play(note);		
	}

	@Override
	public void onKeyReleased(Note note) {
		Playback.getInstance().stop(note);
	}

	@Override
	public View getView() {
		return pianoView;
	}

	@Override
	public void onOctaveUp() {
		Note highestNote = null;
		for(Note note : piano.keys){
			if(highestNote == null){
				highestNote = note;
				continue;
			}
			if(note.number > highestNote.number){
				highestNote = note;
			}
		}
		
		if(highestNote.number <= 96){
			for(Note note : piano.keys){
				note.number += 12;
			}
		}
	}

	@Override
	public void onOctaveDown() {
		Note lowestNote = null;
		for(Note note : piano.keys){
			if(lowestNote == null){
				lowestNote = note;
				continue;
			}
			if(note.number < lowestNote.number){
				lowestNote = note;
			}
		}
		
		if(lowestNote.number > 33){
			for(Note note : piano.keys){
				note.number -= 12;
			}
		}
	}
}