package views;

import java.awt.Color;

import main.MultitouchFramework;

import tracking.Track;

import geometry.Point2D;
import graphics.GraphicsWrapper;

public class TrackView extends View {
	
	public TrackViewListener listener;
	
	protected Track track;
	
	float x2;
	float y2;
	//float length;
	boolean isPlaying = false;
	boolean isPressed = false;
	
	public AnchorView startAnchor;
	public AnchorView endAnchor;
	
	private TrackViewNoteFeedback noteFeedback;
	
	public TrackView(Track track, float x1, float y1) {
		this.track = track;
		this.x = x1;
		this.y = y1;
		this.x2 = 150.0f;
		this.y2 = y1;
		
		//length = (float)Math.sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1));
		
		startAnchor = new AnchorView(x, y, true);
		endAnchor = new AnchorView(x2, y2, false);
		
		noteFeedback = new TrackViewNoteFeedback(this);
	}
	
	public interface TrackViewListener {
		void onLinePressed();
		void onLineReleased();
	}
	
	public interface AnchorViewListener {
		void onPressed();
		void onReleased();
		void onClicked();
		void onDragged();
	}

	@Override
	public void draw(GraphicsWrapper gw) {
		if (isPressed)
		{
			gw.setColor(Color.ORANGE);
		}
		else
		{
			gw.setColor(Color.BLUE);			
		}
		
		noteFeedback.draw(gw);
		
		gw.setLineWidth(3.5f);
		gw.drawLine(startAnchor.center.x(), startAnchor.center.y(), endAnchor.center.x(), endAnchor.center.y());
		
		startAnchor.draw(gw);
		endAnchor.draw(gw);
	}
	
	@Override
	public void moveBy(float dx, float dy) {
		super.moveBy(dx, dy);
		startAnchor.moveBy(dx, dy);
		endAnchor.moveBy(dx, dy);
	}
	
	@Override
	public boolean onTouchEvent(int id, float x, float y, int type) {
		System.out.println("TrackView onTouchEvent");
		super.onTouchEvent(id, x, y, type);
		if (startAnchor.onTouchEvent(id, x, y, type))
		{
			startAnchor.onTouchDown(id, x, type);
			return true;
		}
		
		if (endAnchor.onTouchEvent(id, x, y, type))
		{
			// TODO: drag start anchor to end anchor
			//endAnchor.onEnter();
			return true;
		}
		
		return false;
	}
	
	public class AnchorView extends View/* implements AnchorViewListener*/ {
		
		public static final float RADIUS = 20.0f;
		
		//public AnchorViewListener anchorListener;
		
		private int touchStartCursorId = -1;
		
		boolean isPressed = false;
		boolean isDragged = false;
		private boolean isStartAnchor = false;
		
		public Point2D center;
		
		public AnchorView(float x, float y, boolean isStart)
		{
			this.x = x;
			this.y = y;
			this.isStartAnchor = isStart;
			center = new Point2D(x + RADIUS, y + RADIUS);
		}

		@Override
		public void draw(GraphicsWrapper gw) {
			if (isStartAnchor)
			{
				gw.setColor(Color.GREEN);				
			}
			else
			{
				gw.setColor(Color.RED);
			}
			gw.drawCircle(x, y, RADIUS, true);
		}
		
		@Override
		public void moveBy(float dx, float dy) {
			super.moveBy(dx, dy);
		}
		
		@Override
		public boolean onTouchEvent(int id, float x, float y, int type) {
			if (contains(x, y))
			{
				System.out.println("AnchorView onTouchEvent");
				boolean result = super.onTouchEvent(id, x, y, type);
				if(id == touchStartCursorId){
					if(type == MultitouchFramework.TOUCH_EVENT_UP){
						touchStartCursorId = -1;
					}else if(type == MultitouchFramework.TOUCH_EVENT_MOVE){
						this.onTouchMove(id, x, y);
					}
				}
				return result;
			}
			
			return false;
			
//			if (contains(x, y))
//			{
//				System.out.println("Anchor onTouch event");
//				return true;
//			}
//			
//			return false;
		}
		
		@Override
		public boolean onTouchDown(int id, float x, float y) {
			if (!isPressed)
			{
				touchStartCursorId = id;
				System.out.println("onTouchDown");
				isPressed = true;
				return true;				
			}
			
			return false;
		}
		
		@Override
		public boolean onTouchUp(int id, float x, float y) {
			System.out.println("onTouchUp");
			if (!isDragged && isStartAnchor)
			{
				track.play();				
			}
			isPressed = false;
			isDragged = false;
			return true;
		}
		
		@Override
		public boolean onTouchMove(int id, float x, float y) {
			if (isPressed)
			{
				isDragged = true;
				System.out.println("onTouchMove: " + x + " " + y);
				this.x = x;
				this.y = y;
				center = new Point2D(x + RADIUS, y + RADIUS);
				//moveBy(x, y);
				return true;
			}
			
			return false;
		}
		
		@Override
		public boolean contains(float px, float py) {
			Point2D newPoint = new Point2D(px,py);
			if(center.distance(newPoint) <= 2*RADIUS){
				return true;
			}else{
				return false;
			}
		}

//		@Override
//		public void onClicked() {
//			System.out.println("onClicked called");
//			track.play();
//		}
//
//		@Override
//		public void onPressed() {
//			System.out.println("onPressed called");
//		}
//
//		@Override
//		public void onReleased() {
//			System.out.println("onReleased called");
//		}
//
//		@Override
//		public void onDragged() {
//			System.out.println("onDragged called");
//		}
	}
}
