package views;

import graphics.GraphicsWrapper;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import models.Piano;
import playback.Note;
import views.Button.OnClickListener;

public class PianoView extends InstrumentView {

	public static final int KEY_WIDTH = 50;

	public PianoViewListener listener;

	protected Piano piano;
	protected List<KeyView> keyViews = new ArrayList<KeyView>();

	public Button btnOctaveUp;
	public Button btnOctaveDown;
	
	public PianoView(Piano piano, float x, float y, float h){
		super(x,y,0,h);
		this.piano = piano;
		this.x = x;
		this.y = y;
		this.height = h;

		KeyView lastKeyView = null;
		for(Note note : piano.keys){
			KeyView kv = new KeyView(this);
			kv.note = note;

			kv.y = this.y;

			if(note.isWhite()){
				kv.height = this.height;
			}else{
				kv.height = this.height - 50;
			}

			kv.width = KEY_WIDTH;

			if(lastKeyView == null){
				kv.x = this.x;
			}else if(note.isWhite()){
				if(lastKeyView.note.isWhite()){
					kv.x = lastKeyView.x + KEY_WIDTH;
				}else{
					kv.x = lastKeyView.x + (KEY_WIDTH/2.0f);
				}
			}else{
				if(lastKeyView.note.isWhite()){
					kv.x = lastKeyView.x + (KEY_WIDTH/2.0f);
				}else{
					kv.x = lastKeyView.x + KEY_WIDTH;
				}
			}

			keyViews.add(kv);
			lastKeyView = kv;
		}

		if(keyViews.size() > 1){
			KeyView last = keyViews.get(keyViews.size()-1);
			KeyView first = keyViews.get(0);
			this.width = last.x+last.width - first.x;
		}else{
			this.width = KEY_WIDTH;
		}
		
		btnOctaveDown = new Button("OCTAVE -", btnMove.x+100, y-30, this);
		btnOctaveUp = new Button("OCTAVE +", btnOctaveDown.x+100, y-30, this);
		
		btnOctaveDown.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(Button button) {
				if(listener != null){
					listener.onOctaveDown();
				}
			}
		});
		
		btnOctaveUp.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(Button button) {
				if(listener != null){
					listener.onOctaveUp();
				}
			}
		});
	}

	public interface PianoViewListener{
		void onKeyPressed(Note note);
		void onKeyReleased(Note note);
		void onOctaveUp();
		void onOctaveDown();
	}

	@Override
	public void draw(GraphicsWrapper gw) {
		
//		angle -= 0.1f;
		
		gw.beginRotate(angle,x,y);

		gw.beginRotate(0, -x, -y);
		btnOctaveDown.draw(gw);
		btnOctaveUp.draw(gw);
		super.draw(gw);
		gw.endRotate();
		
		gw.setColor(Color.black);
		gw.drawRect(0, 0, width, height);

		for(KeyView kv : keyViews){
			if(kv.note.isWhite()){
				kv.draw(gw);
			}
		}
		for(KeyView kv : keyViews){
			if(!kv.note.isWhite()){
				kv.draw(gw);
			}
		}		
		gw.endRotate();
	}
	
	@Override
	public void moveBy(float dx, float dy) {
		super.moveBy(dx, dy);
		for(KeyView kv : keyViews){
			kv.moveBy(dx, dy);
		}
		btnOctaveDown.moveBy(dx, dy);
		btnOctaveUp.moveBy(dx, dy);
	}

	@Override
	public boolean onTouchEvent(int id, float x, float y, int type) {
		super.onTouchEvent(id, x, y, type);
		for(KeyView kv : keyViews){
			if(!kv.note.isWhite()){
				if(kv.onTouchEvent(id, x, y, type)){
					return true;
				}
			}
		}
		for(KeyView kv : keyViews){
			if(kv.note.isWhite()){
				if(kv.onTouchEvent(id, x, y, type)){
					return true;
				}
			}
		}
		
		btnOctaveDown.onTouchEvent(id, x, y, type);
		btnOctaveUp.onTouchEvent(id, x, y, type);
		
		return false;
	}

	public class KeyView extends View{
		Note note;

		boolean isPressed = false;

		public KeyView(PianoView pianoView) {
			super(pianoView);
		}

		@Override
		public void draw(GraphicsWrapper gw) {
						
			float rx = this.x - parent.x;
			float ry = this.y - parent.y;
			
			if(note.isWhite()){
				gw.setColor(Color.black);
				gw.drawRect(rx, ry, width, height);
				if(isPressed){
					gw.setColor(Color.blue);
				}else{
					gw.setColor(Color.white);
				}
				gw.fillRect(rx, ry, width, height);
				gw.setColor(Color.black);
				gw.drawString(rx+10, ry-10+height, note.toString());
			}else{
				if(isPressed){
					gw.setColor(Color.blue);
				}else{
					gw.setColor(Color.black);
				}
				gw.fillRect(rx+10, ry, width-10, height);
				gw.setColor(Color.white);
				gw.drawString(rx+20, ry-10+height, note.toString());
			}
		}

		@Override
		public boolean onTouchDown(int id, float x, float y) {
			if(!isPressed){
				listener.onKeyPressed(note);
				isPressed = true;
				return true;
			}
			return false;
		}
		
		@Override
		public boolean onTouchUp(int id, float x, float y) {
			isPressed = false;
			listener.onKeyReleased(note);
			return true;
		}
		
		@Override
		public boolean onTouchEnter(int id, float x, float y) {
			if(!isPressed){
				listener.onKeyPressed(note);
				isPressed = true;
				return true;
			}
			return false;
		}

		@Override
		public boolean onTouchExit(int id, float x, float y) {
			isPressed = false;
			listener.onKeyReleased(note);
			return true;
		}
	}
}