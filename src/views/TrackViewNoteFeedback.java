package views;

import graphics.GraphicsWrapper;

import java.util.ArrayList;

import playback.Note;
import tracking.Track.TrackListener;

public class TrackViewNoteFeedback implements TrackListener {

	TrackView trackView;
	
	private ArrayList<NoteFeedbackView> noteFeedbackViews = new ArrayList<NoteFeedbackView>();
	private ArrayList<NoteFeedbackView> noteFeedbackViewsToRemove = new ArrayList<NoteFeedbackView>();
	
	private final float SPEED = 0.2f;
	
	public TrackViewNoteFeedback(TrackView trackView){
		this.trackView = trackView;
		this.trackView.track.listener = this;
	}
	
	public void draw(GraphicsWrapper gw){
		
		float rx = trackView.endAnchor.center.x() - trackView.startAnchor.center.x();
		float ry = trackView.endAnchor.center.y() - trackView.startAnchor.center.y();
		
		for(NoteFeedbackView noteFeedbackView : noteFeedbackViews){
			
			noteFeedbackView.progress += SPEED;
			
			float x = rx*noteFeedbackView.progress + trackView.startAnchor.center.x();
			float y = ry*noteFeedbackView.progress + trackView.startAnchor.center.y();
			
			gw.setColor(1f, 0.5f, 1.0f);
			gw.drawCenteredCircle(x, y, 5.0f, false);
			
			if(noteFeedbackView.progress > 1.0f){
				noteFeedbackViewsToRemove.add(noteFeedbackView);
			}
		}
		
		noteFeedbackViews.removeAll(noteFeedbackViewsToRemove);
	}

	@Override
	public void onNoteCuePlayed(Note note) {
		NoteFeedbackView noteFeedBackView = new NoteFeedbackView();
		noteFeedBackView.note = note;
		noteFeedbackViews.add(noteFeedBackView);
	}
	
	private class NoteFeedbackView{
		float progress=0;
		Note note= null;
	}
}
