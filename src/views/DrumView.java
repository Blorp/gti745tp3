package views;

import playback.Note;
import geometry.Point2D;
import graphics.GraphicsWrapper;
import models.Drum;

public class DrumView extends InstrumentView {

	public DrumViewListener listener;
	protected Drum drum;
	private float radius;
	boolean isPressed = false;
	private Point2D middlePoint;
	
	public DrumView(Drum drum, float x, float y, float radius){
		super(x,y,0,0);
		this.drum = drum;
		this.radius = radius;
		this.middlePoint = new Point2D(x+radius,y+radius);
	}
	
	public interface DrumViewListener{
		void onKeyPressed();
		void onKeyReleased();
	}

	@Override
	public void draw(GraphicsWrapper gw) {
		super.draw(gw);
		if(radius <= 30){
			gw.setColor(0.5f,0.75f,0.5f,0.65f);	
			drum.currentKey = drum.keys.get(0);
		}else if (radius <= 50) {
			gw.setColor(0.5f,0.45f,0.95f,0.65f);
			drum.currentKey = drum.keys.get(1);
		}else if (radius <= 70) {
			gw.setColor(0.15f,0.45f,0.15f,0.65f);
			drum.currentKey = drum.keys.get(2);
		}else if (radius <= 90) {
			gw.setColor(0.5f,0.45f,0.35f,0.35f);
			drum.currentKey = drum.keys.get(3);
		}else if (radius <= 110) {
			gw.setColor(0.33f,0.22f,0.22f,0.35f);
			drum.currentKey = drum.keys.get(4);
		}else{
			gw.setColor(0.33f,0.22f,0.22f,0.35f);
			drum.currentKey = drum.keys.get(4);
			radius = 110;	
		}
		
		gw.drawCircle(x, y, radius, true);	
	}
	
	@Override
	public boolean onTouchEvent(int id, float x, float y, int type){
		return super.onTouchEvent(id, x, y, type);
	}
	
	public boolean contains(float px, float py) {
		Point2D newPoint = new Point2D(px,py);
		if(middlePoint.distance(newPoint) <= radius){
			return true;
		}else{
			return false;
		}
		
	}
	
	@Override
	public void moveBy(float dx, float dy) {
		super.moveBy(dx, dy);
		middlePoint = new Point2D(x+radius+dx,y+radius+dx);
	}

	
	@Override
	public boolean onTouchDown(int id, float x, float y) {
		if(!isPressed){
			listener.onKeyPressed();
			isPressed = true;
			return true;
		}
		return false;
	}
	
	
	@Override
	public boolean onTouchUp(int id, float x, float y) {
		listener.onKeyReleased();
		isPressed = false;
		return true;
	}

	
}