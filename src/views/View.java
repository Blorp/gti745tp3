package views;

import graphics.GraphicsWrapper;
import main.MultitouchFramework;

public abstract class View {
	public float x;
	public float y;
	public float angle;
	public float width;
	public float height;

	protected View parent;

	//TODO: REMOVE THIS
	public float containsX;
	public float containsY;

	private boolean touchInside = false;
	private int touchInsideId = -1;

	public View(){};

	public View(View parent){
		this.parent = parent;
	}

	public abstract void draw(GraphicsWrapper gw);

	public void moveBy(float dx, float dy){
		this.x += dx;
		this.y += dy;
	}

	public boolean onTouchEvent(int id, float x, float y, int type){
		if(this.contains(x, y)){
			if(type == MultitouchFramework.TOUCH_EVENT_DOWN){
				touchInside = true;
				touchInsideId = id;
				return onTouchDown(id,x,y);
			}else if(type == MultitouchFramework.TOUCH_EVENT_UP){
				touchInside = false;
				touchInsideId = -1;
				return onTouchUp(id,x,y);
			}else if(type == MultitouchFramework.TOUCH_EVENT_MOVE){
				if(!touchInside){
					onTouchEnter(id,x,y);
				}
				touchInside = true;
				touchInsideId = id;
				return onTouchMove(id,x,y);
			}
		}else{
			if(type == MultitouchFramework.TOUCH_EVENT_MOVE){
				if(touchInside && touchInsideId == id){
					onTouchExit(id,x,y);
				}
				touchInside = false;
				touchInsideId = -1;
			}
		}
		return false;
	}

	public boolean onTouchDown(int id, float x, float y){
		//To implement in subclasses
		return false;
	}

	public boolean onTouchUp(int id, float x, float y){
		//To implement in subclasses
		return false;
	}

	public boolean onTouchMove(int id, float x, float y){
		//To implement in subclasses
		return false;
	}

	public boolean onTouchEnter(int id, float x, float y){
		//To implement in subclasses
		return false;	
	}

	public boolean onTouchExit(int id, float x, float y){
		//To implement in subclasses
		return false;
	}

	public boolean contains(float px, float py) {
		float a = getAngle();
		float cx = parent != null ? parent.x : x;
		float cy = parent != null ? parent.y : y;
		//		if(a != 0){
		px -= cx;
		py -= cy;
		double c  = Math.cos(a);
		double s = Math.sin(a);
		float tempx = px;
		float tempy = py;
		px = (float) (tempx*c + tempy*s);
		py = (float) (-tempx*s + tempy*c);
		px += cx;
		py += cy;
		containsX = px;
		containsY = py;
		//		}
		boolean result = (px > x && px < x+width && py > y && py < y+height);
		return result;
	}

	public float getAngle(){
		if(parent != null){
			return angle + parent.getAngle();
		}else{
			return angle;
		}
	}

	public boolean isPressingButton(float px, float py){
		//To implement in subclasses
		return false;
	}
}
