package views;

import java.awt.Color;
import java.util.ArrayList;

import geometry.Point2D;
import graphics.GraphicsWrapper;

public class MusicalSurfaceView extends InstrumentView {

	public MusicalSurfaceViewListener listener;
	//protected MusicalSurface musicalSurface;
	boolean isPressed = false;
	private float w,h;
	final int max_channel = 15;
	final int max_number = 128;
	private int channel;
	private int number;
	
	
	public MusicalSurfaceView(float x,float y,float  w,float h){
		super(x,y,0,0);
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
	}
	
	public interface MusicalSurfaceViewListener{
		void onKeyPressed(int number, int channel);
		void onKeyReleased(int number, int channel);
	}
	
	@Override
	public void draw(GraphicsWrapper gw) {
		super.draw(gw);
		gw.setColor(Color.red);

		
		gw.drawRect(x, y, w, h, true);
		//gw.drawPolygon(points,true);
	}

	@Override
	public boolean onTouchEvent(int id, float x, float y, int type){
		return super.onTouchEvent(id, x, y, type);
	}
	
	@Override
	public void moveBy(float dx, float dy) {
		super.moveBy(dx, dy);
	}
	
	public boolean contains(float px, float py) {
		if(px >= x && px <= x+w && py >= y && py <= y+h){
			return true;
		}else{
			return false;
		}
	}
	
	@Override
	public boolean onTouchDown(int id, float x, float y) {
		if(!isPressed){
			channel =  Math.round(((x - this.x)/w)*max_channel);
			number =  Math.round(((y - this.y)/h)*max_number);
			listener.onKeyPressed(number, channel);
			isPressed = true;
			return true;
		}
		return false;
	}
	
	
	@Override
	public boolean onTouchUp(int id, float x, float y) {
		listener.onKeyReleased(number, channel);
		isPressed = false;
		return true;
	}
	
	@Override
	public boolean onTouchMove(int id, float x, float y){
		listener.onKeyReleased(number, channel);
		channel =  Math.round(((x - this.x)/w)*max_channel);
		number =  Math.round(((y - this.y)/h)*max_number);
		listener.onKeyPressed(number, channel);
		isPressed = true;
		return true;
	}
	
	@Override
	public boolean onTouchEnter(int id, float x, float y) {
		if(!isPressed){
			channel =  Math.round(((x - this.x)/w)*max_channel);
			number =  Math.round(((y - this.y)/h)*max_number);
			listener.onKeyPressed(number, channel);
			isPressed = true;
			return true;
		}
		return false;
	}
	
	
	@Override
	public boolean onTouchExit(int id, float x, float y) {
		listener.onKeyReleased(number, channel);
		isPressed = false;
		return true;
	}
	
	
}