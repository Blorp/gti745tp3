package views;

import main.MultitouchFramework;
import graphics.GraphicsWrapper;

public class Button extends View {

	public String text;
	public float padding = 5;
	private OnClickListener onClickListener;
	private OnDragListener onDragListener;
	private boolean isSelected = false;
	private float oldTouchX;
	private float oldTouchY;
	
	private int touchStartCursorId = -1;
	
	public Button(String text, float x, float y){
		this.text = text;
		this.x = x;
		this.y = y;
		this.height = 20;
	}
	
	public Button(String text, float x, float y, View parent){
		this(text, x, y);
		this.parent = parent;
	}
	
	@Override
	public void draw(GraphicsWrapper gw) {
		this.width = gw.stringWidth(text);
		
		if(isSelected){
			gw.setColor(0, 0, 1);
			gw.fillRect(x-padding, y-padding, width+2*padding, height+2*padding);	
		}
		gw.setColor(0, 0, 0);
		gw.drawString(x, y+0.66f*height, text);
		gw.setColor(0, 0, 0);
		gw.drawRect(x-padding, y-padding, width+2*padding, height+2*padding);
	}
	
	public void setOnClickListener(OnClickListener l){
		this.onClickListener = l;
	}
	
	public void setOnDragListener(OnDragListener l){
		this.onDragListener = l;
	}
	
	@Override
	public boolean onTouchEvent(int id, float x, float y, int type) {
		boolean result = super.onTouchEvent(id, x, y, type);
		if(id == touchStartCursorId){
			if(type == MultitouchFramework.TOUCH_EVENT_UP){
				touchStartCursorId = -1;
			}else if(type == MultitouchFramework.TOUCH_EVENT_MOVE){
				// Add touch move event outside bounds if the touch started inside
				if(!contains(x, y)){
					this.onTouchMove(id, x, y);
				}
			}
		}
		return result;
	}

	@Override
	public boolean onTouchMove(int id, float x, float y) {
		if(onDragListener != null){
			onDragListener.onDrag(this, x-oldTouchX, y-oldTouchY);
		}
		oldTouchX = x;
		oldTouchY = y;
		return super.onTouchMove(id, x, y);
	}

	@Override
	public boolean onTouchDown(int id, float x, float y) {
		isSelected = true;
		oldTouchX = x;
		oldTouchY = y;
		touchStartCursorId = id;
		return super.onTouchDown(id, x, y);
	}

	@Override
	public boolean onTouchEnter(int id, float x, float y) {
		isSelected = true;
		return super.onTouchEnter(id, x, y);
	}

	@Override
	public boolean onTouchExit(int id, float x, float y) {
		isSelected = false;
		return super.onTouchExit(id, x, y);
	}

	@Override
	public boolean onTouchUp(int id, float x, float y) {
		isSelected = false;
		if(onClickListener != null){
			onClickListener.onClick(this);
		}
		return super.onTouchUp(id, x, y);
	}
	
	public interface OnClickListener{
		void onClick(Button button);
	}
	
	public interface OnDragListener{
		void onDrag(Button button, float dx, float dy);
	}	
}
