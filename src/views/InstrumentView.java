package views;

import graphics.GraphicsWrapper;
import views.Button.OnDragListener;

public abstract class InstrumentView extends View {

	public Button btnDelete;
	public Button btnMove;
	
	public InstrumentView(float x, float y, float width, float height){
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		btnDelete = new Button("DELETE", x, y-30, this);
		btnMove = new Button("MOVE", x +80, y-30, this);
		btnMove.setOnDragListener(new OnDragListener(){
			@Override
			public void onDrag(Button button, float dx, float dy) {
				moveBy(dx,dy);
			}
		});
	}
	
	@Override
	public void draw(GraphicsWrapper gw) {
		btnDelete.draw(gw);
		btnMove.draw(gw);
	}
	
	@Override
	public void moveBy(float dx, float dy) {
		super.moveBy(dx, dy);
		btnDelete.moveBy(dx, dy);
		btnMove.moveBy(dx, dy);
	}

	@Override
	public boolean onTouchEvent(int id, float x, float y, int type) {
		btnDelete.onTouchEvent(id, x, y, type);
		btnMove.onTouchEvent(id, x, y, type);
		return super.onTouchEvent(id, x, y, type);
	}
	
	public boolean isPressingButton(float px, float py){
		if(btnMove.contains(px, py)){
			return true;
		}else{
			return false;
		}
	}
}
