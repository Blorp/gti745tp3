package views;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;

import models.Piano;
import playback.Note;

public class PianoViewTest extends PianoView implements KeyListener{

	Note testNote = Note.get("C5");
	
	public PianoViewTest(Piano piano) {
		super(piano,10,10,300);
	}

	public void show(){
		JFrame frame = new JFrame("PianoViewTest");
		frame.setPreferredSize(new Dimension(500,200));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBackground(Color.BLUE);
		frame.addKeyListener(this);
		frame.pack();
		frame.setVisible(true);
	}
	
	@Override
	public void keyPressed(KeyEvent key) {
		if(listener != null){
			listener.onKeyPressed(piano.keys.get(keyIndexOfChar(key.getKeyChar())));
		}
	}

	@Override
	public void keyReleased(KeyEvent key) {
		if(listener != null){
			listener.onKeyReleased(piano.keys.get(keyIndexOfChar(key.getKeyChar())));
		}
	}

	@Override
	public void keyTyped(KeyEvent key) {
	}
	
	private int keyIndexOfChar(char c){
		switch(c){
		case 'z':
			return 0;//c
		case 'x':
			return 1;//d
		case 'c':
			return 2;//e
		case 'v':
			return 3;//f
		case 'b':
			return 4;//g
		case 'n':
			return 5;//a
		case 'm':
			return 6;//b
		case ',':
			return 7;//b
		case 's':
			return 8;//c#
		case 'd':
			return 9;//d#
		case 'g':
			return 10;//f#
		case 'h':
			return 11;//g#
		case 'j':
			return 12;//a#
		}
		return 0;
	}
}
